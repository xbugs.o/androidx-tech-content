---
docs:
articles:
  -
    title: Theming with AppCompat (2016-01-21)
    url: https://medium.com/androiddevelopers/theming-with-appcompat-1a292b754b35
    description: Article on how to customize your AppCompat theme to control your app's look and feel
samples:
  -
    title: '*Exploring Android*'
    url: https://gitlab.com/commonsguy/cw-andexplore
    description: CommonsWare's hands-on tutorial book builds a complete AppCompat app from scratch
  -
    title: Sunflower
    url: https://github.com/googlesamples/android-sunflower
    description: A gardening app illustrating Android development best practices with Android
  - 
    title: Google's Architecture Samples
    url: https://github.com/googlesamples/android-architecture
--- |
    AppCompat is a way to get a consistent [Material Design](https://material.io/)
    look across Android OS versions, particularly to versions older than Android 5.0.

    While there are dozens of classes in this artifact, the only one that you will
    work with directly (usually) is `AppCompatActivity`. If you have your activity
    inherit from `AppCompatActivity`, you are requesting to use AppCompat and
    to adopt the Material Design look.

    If you inherit from `AppCompatActivity`, you also need to:

    - Have a theme that extends from `Theme.AppCompat` (or a sub-theme)

    - Use `app:` namespaced attributes for some things in menu and layout resources
    